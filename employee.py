# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta


class Employee(metaclass=PoolMeta):
    __name__ = "company.employee"
    guard = fields.Boolean('Guard')
    skills = fields.Text('Skills')
    notes = fields.Text('Notes')
    tags = fields.Many2Many('company.employee-surveillance.tag',
        'employee', 'tag', 'Tags')
    city = fields.Function(fields.Char('City'), 'get_city')

    def get_city(self, name):
        if self.party:
            return self.party.city_name


class EmployeeTag(ModelSQL):
    'Employee - Tag'
    __name__ = 'company.employee-surveillance.tag'
    _table = 'company_employee_surveillance_tag_rel'
    employee = fields.Many2One('company.employee', 'Employee',
        ondelete='CASCADE', select=True, required=True)
    tag = fields.Many2One('surveillance.tag', 'Tag',
        ondelete='RESTRICT', select=True, required=True)
