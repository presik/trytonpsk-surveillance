# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import configuration
from . import schedule
from . import employee
from . import plan
from . import access
from . import payroll
from . import sale
from . import tag
from . import dash
from . import operation


def register():
    Pool.register(
        configuration.Configuration,
        tag.Tag,
        employee.Employee,
        employee.EmployeeTag,
        schedule.SurveillanceSchedule,
        schedule.ScheduleShift,
        schedule.CreateScheduleShiftStart,
        schedule.ScheduleMonthStart,
        schedule.ShiftExtraPayment,
        plan.Position,
        plan.SurveillanceLocation,
        plan.PlanGuard,
        plan.SurveillancePlan,
        plan.SurveillancePlanLine,
        plan.PlanShiftsPattern,
        plan.PlanShiftsWeek,
        plan.LocationTag,
        plan.ExtraPayment,
        access.CreateAccessFromShiftStart,
        access.StaffAccess,
        access.StaffAccessExtraPayment,
        payroll.Payroll,
        sale.ContractProductLine,
        sale.SaleContract,
        sale.ContractProductPosition,
        dash.DashApp,
        dash.AppSurveillanceSchedule,
        operation.Audit,
        operation.AuditStart,
        module='surveillance', type_='model')
    Pool.register(
        schedule.ScheduleMonth,
        schedule.CreateScheduleShift,
        access.CreateAccessFromShift,
        operation.AuditWizard,
        module='surveillance', type_='wizard')
    Pool.register(
        schedule.ScheduleMonthReport,
        module='surveillance', type_='report')
