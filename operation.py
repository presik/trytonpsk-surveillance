# This file is part of Presik.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import timedelta, date

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, Button, StateTransition


class Audit(ModelSQL, ModelView):
    'Surveillance Audit'
    __name__ = 'surveillance.audit'
    company = fields.Many2One('company.company', 'Company', required=True,
        readonly=True)
    audit_date = fields.Date('Date', required=True, readonly=True)


class AuditStart(ModelView):
    'Audit Start'
    __name__ = 'surveillance.audit.start'
    company = fields.Many2One('company.company', 'Company', required=True)


class AuditWizard(Wizard):
    'Audit Wizard'
    __name__ = 'surveillance.audit.wizard'
    """
    This is the wizard for audit daily
    """
    start = StateView(
        'surveillance.audit.start',
        'surveillance.audit_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok'),
        ]
    )
    accept = StateTransition()

    def close_shifts(self):
        pool = Pool()
        Date = pool.get('ir.date')
        Shift = pool.get('surveillance.schedule.shift')
        two_days_ago = Date.today() - timedelta(days=2)
        shifts = Shift.search([
            ('shift_date', '<=', two_days_ago),
            ('state', '=', 'draft'),
        ])
        Shift.write(shifts, {'state': 'performed'})

    def inactive_plans(self):
        pool = Pool()
        Date = pool.get('ir.date')
        PlanLine = pool.get('surveillance.plan.line')
        lines = PlanLine.search([
            ('end_date', '>=', Date.today()),
            ('state', 'in', ('operative', 'preoperative')),
        ])
        PlanLine.write(lines, {'state': 'inactive'})

    def add_staff_events(self):
        pool = Pool()
        Date = pool.get('ir.date')
        Event = pool.get('staff.event')
        Shift = pool.get('surveillance.schedule.shift')
        shifts = Shift.search([
            ('shift_date', '<=', Date.today()),
            ('event_category', '!=', None),
            ('event', '=', None),
        ])
        for shift in shifts:
            event, = Event.create([{
                'employee': shift.guard.id,
                'category': shift.event_category.id,
                'event_date': shift.shift_date,
                'start_date': shift.shift_date,
                'reference': shift.schedule.location.name,
                'quantity': 1,
                'state': 'draft',
            }])
            shift.event = event.id
            shift.save()

    def register_access_shift(self):
        Access = Pool().get('staff.access')
        start_date = date.today() - timedelta(days=30)
        end_date = date.today() - timedelta(days=2)
        Access.create_access_from(start_date, end_date)

    def transition_accept(self):
        pool = Pool()
        Audit = pool.get('surveillance.audit')
        Date = pool.get('ir.date')
        self.close_shifts()
        self.inactive_plans()
        self.add_staff_events()
        self.register_access_shift()
        Audit.create([{
            'company': self.start.company.id,
            'audit_date': Date.today(),
        }])
        return 'end'
