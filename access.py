# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.pool import Pool, PoolMeta


class StaffAccess(metaclass=PoolMeta):
    __name__ = "staff.access"
    extra_payments = fields.One2Many('staff.access.extra_payment',
        'access', 'Extra Payments')
    extra_shift = fields.Boolean('Extra Shift')
    wage_type_extra_shift = fields.Many2One('staff.wage_type',
        'Wage Type Extra Shift')

    @classmethod
    def create_access_from(cls, start_date, end_date):
        ScheduleShift = Pool().get('surveillance.schedule.shift')
        shifts = ScheduleShift.search([
            ('shift_date', '>=', start_date),
            ('shift_date', '<=', end_date),
            ('access', '=', None),
        ])

        for shift in shifts:
            # if not shift.guard.contract:
            #     raise UserError(gettext(
            #         'surveillance.msg_employee_without_contract'
            #     ))
            if not shift.kind or shift.type != 'work':
                continue
            if (shift.exit_dtime - shift.enter_dtime).seconds < 1800:
                continue

            extra_payments = []
            value_ = {
                'employee': shift.guard.id,
                'enter_timestamp': shift.enter_dtime,
                'exit_timestamp': shift.exit_dtime,
                'shift_kind': shift.kind.id,
                'shift_amount': shift.shift_amount or 0,
                'shift_amount_add': 0,
                'payment_method': shift.payment_method,
            }
            if shift.extra_shift and shift.position.wage_type_extra_shift:
                value_['extra_shift'] = shift.extra_shift
                value_['wage_type_extra_shift'] = shift.position.wage_type_extra_shift.id

            for expay in shift.extra_payments:
                extra_payments.append({
                    'wage_type': expay.wage_type.id,
                    'amount': expay.amount,
                })
            if extra_payments:
                value_['extra_payments'] = [('create', extra_payments)]
            access, = cls.create([value_])
            ScheduleShift.write([shift], {'access': access.id})


class StaffAccessExtraPayment(ModelSQL, ModelView):
    'Staff Extra Payment'
    __name__ = 'staff.access.extra_payment'
    access = fields.Many2One('staff.access', 'Access', required=True,
        ondelete='CASCADE')
    wage_type = fields.Many2One('staff.wage_type', 'Wage Type', required=True)
    amount = fields.Numeric('Amount', digits=(16, 2), required=True)


class CreateAccessFromShiftStart(ModelView):
    'Create Access From Shift Start'
    __name__ = 'surveillance.create_access_shift.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)


class CreateAccessFromShift(Wizard):
    'Create Access From Shift'
    __name__ = 'surveillance.create_access_shift'
    start = StateView(
        'surveillance.create_access_shift.start',
        'surveillance.create_access_shift_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'do_', 'tryton-ok', default=True),
        ])
    do_ = StateTransition()

    def transition_do_(self):
        Access = Pool().get('staff.access')
        Access.create_access_from(self.start.start_date, self.start.end_date)
        return 'end'
