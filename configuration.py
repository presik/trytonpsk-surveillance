# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction


class Configuration(ModelSQL, ModelView):
    'Surveillance Configuration'
    __name__ = 'surveillance.configuration'
    company = fields.Many2One('company.company', 'Company')
    work_time_limit = fields.Integer('Work-Time Limit', help='In hours')
    wage_type_extra_compensation = fields.Many2One('staff.wage_type',
        'Wage Type Extra Compensation')
    wage_type_salary = fields.Many2One('staff.wage_type', 'Wage Type Salary')
    wage_type_shift_fixed = fields.Many2One('staff.wage_type',
        'Wage Type Shift Fixed')
    shift_limit = fields.Integer('Number Shift Limit')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')
