# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, If, In, Get
from trytond.transaction import Transaction
from .schedule import PAYMENT_METHOD

STATES = {'readonly': (Eval('state') != 'draft')}


class SurveillanceLocation(ModelSQL, ModelView):
    "Surveillance Location"
    __name__ = "surveillance.location"
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    risk = fields.Selection([
            ('low', 'Low'),
            ('middle', 'Middle'),
            ('high', 'High'),
        ], 'Risk', required=True)
    category = fields.Selection([
            ('commercial', 'Commercial'),
            ('residential', 'Residential'),
            ('official', 'Official'),
            ('financial', 'Financial'),
            ('health', 'Health'),
        ], 'Category', required=True)
    video_cams = fields.Boolean('Video Cams')
    city = fields.Many2One('party.city_code', 'City')
    positions = fields.One2Many('surveillance.location.position', 'location',
        'Positions')
    customer = fields.Many2One('party.party', 'Customer', select=True,
        required=False)
    tags = fields.Many2Many('surveillance.location-surveillance.tag',
        'location', 'tag', 'Tags')
    operation_center = fields.Many2One('company.operation_center', 'OC',
        required=False)

    @classmethod
    def __setup__(cls):
        super(SurveillanceLocation, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @staticmethod
    def default_risk():
        return 'middle'

    @staticmethod
    def default_category():
        return 'residential'


class LocationTag(ModelSQL):
    'Guard - Tag'
    __name__ = 'surveillance.location-surveillance.tag'
    _table = 'surveillance_location_surveillance_tag_rel'
    location = fields.Many2One('surveillance.location', 'Location',
        ondelete='CASCADE', select=True, required=True)
    tag = fields.Many2One('surveillance.tag', 'Tag',
        ondelete='RESTRICT', select=True, required=True)


class Position(ModelSQL, ModelView):
    'Position'
    __name__ = 'surveillance.location.position'
    location = fields.Many2One('surveillance.location', 'Location',
        required=True)
    name = fields.Char('Name', required=True)
    quantity = fields.Integer('Quantity', required=True)
    shift_amount = fields.Numeric('Shift Amount', digits=(16, 2))
    extra_shift_amount = fields.Numeric('Extra Shift Amount', digits=(16, 2))
    amount_extra = fields.Function(fields.Numeric('Extra Payment',
        digits=(16, 2)), 'get_amount_extra')
    wage_type_extra_shift = fields.Many2One('staff.wage_type',
        'Wage Type Extra Shift', required=True)
    extra_payments = fields.One2Many('surveillance.extra_payment', 'position',
        'Extra Payments in Shift')

    @staticmethod
    def default_quantity():
        return 1

    def get_amount_extra(self, name=None):
        return sum(extra.amount for extra in self.extra_payments)


class ExtraPayment(ModelSQL, ModelView):
    'Extra Payment'
    __name__ = 'surveillance.extra_payment'
    position = fields.Many2One('surveillance.location.position', 'Position',
        required=True)
    wage_type = fields.Many2One('staff.wage_type', 'Wage Type',
        required=True)
    amount = fields.Numeric('Amount', digits=(16, 2), required=True)


class SurveillancePlan(Workflow, ModelSQL, ModelView):
    'Surveillance Plan'
    __name__ = 'surveillance.plan'
    _rec_name = 'code'
    code = fields.Char('Code', required=False)
    customer = fields.Many2One('party.party', 'Customer', select=True,
        required=True, states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
        states=STATES, domain=[('id', If(In('company',
        Eval('context', {})), '=', '!='), Get(Eval('context', {}),
        'company', 0))])
    lines = fields.One2Many('surveillance.plan.line', 'plan', 'Plan Lines',
        states=STATES)
    notes = fields.Text('Notes', states=STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('running', 'Running'),
        ('finished', 'Finished'),
        ('cancelled', 'Cancelled'),
    ], 'State', required=True)

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_active():
        return True


class SurveillancePlanLine(ModelSQL, ModelView):
    'Surveillance Plan Line'
    __name__ = 'surveillance.plan.line'
    _rec_name = 'location.name'
    plan = fields.Many2One('surveillance.plan', 'Plan', required=True)
    location = fields.Many2One('surveillance.location', 'Location',
        required=True)
    operation_center = fields.Many2One('company.operation_center', 'OC',
        required=False)
    contract = fields.Many2One('sale.contract', 'Contract', select=True,
        states={'readonly': True})
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    authorized_gun = fields.Boolean('Authorized Gun')
    radio_number = fields.Integer('Radio Number')
    guards = fields.One2Many('surveillance.plan.guard', 'line', 'Guards')
    shifts_pattern = fields.One2Many('surveillance.plan.shift_pattern',
        'line', 'Shifts Pattern', context={
            'location': Eval('location', -1)
        })
    shifts_week = fields.One2Many('surveillance.plan.shift_week',
        'line', 'Shifts Week', context={
            'location': Eval('location', -1)
        })
    hired_service = fields.Many2One('sale.contract.product_line',
        'Hired Service', required=False, states={
            'readonly': False,
        })
    back_staff = fields.Integer('Back Staff')
    comment = fields.Text('Comment')
    state = fields.Selection([
            ('preoperative', 'Preoperative'),
            ('operative', 'Operative'),
            ('inactive', 'Inactive'),
        ], 'State', required=True)
    shift_payment_method = fields.Selection(PAYMENT_METHOD, 'Payment Method')

    @staticmethod
    def default_state():
        return 'preoperative'


class PlanGuard(ModelSQL, ModelView):
    'Plan Guard'
    __name__ = 'surveillance.plan.guard'
    _rec_name = 'guard'
    line = fields.Many2One('surveillance.plan.line', 'Line', required=True)
    guard = fields.Many2One('company.employee', 'Guard', required=True,
        domain=[('guard', '=', True)])
    profile = fields.Selection([
            ('main', 'Main'),
            ('back', 'Back'),
        ], 'Profile', required=True)

    @staticmethod
    def default_profile():
        return 'main'


class PlanShiftsPattern(ModelSQL, ModelView):
    'Plan Shifts Pattern'
    __name__ = 'surveillance.plan.shift_pattern'
    _rec_name = 'sequence'
    line = fields.Many2One('surveillance.plan.line', 'Line', required=True)
    sequence = fields.Integer('Sequence', required=True)
    guard = fields.Many2One('company.employee', 'Guard')
    position = fields.Many2One('surveillance.location.position', 'Position',
        domain=[
            ('location', '=', Eval('_parent_line', {}).get('location')),
        ])
    day01 = fields.Many2One('staff.shift.kind', 'S01')
    day02 = fields.Many2One('staff.shift.kind', 'S02')
    day03 = fields.Many2One('staff.shift.kind', 'S03')
    day04 = fields.Many2One('staff.shift.kind', 'S04')
    day05 = fields.Many2One('staff.shift.kind', 'S05')
    day06 = fields.Many2One('staff.shift.kind', 'S06')
    day07 = fields.Many2One('staff.shift.kind', 'S07')
    day08 = fields.Many2One('staff.shift.kind', 'S08')
    day09 = fields.Many2One('staff.shift.kind', 'S09')
    day10 = fields.Many2One('staff.shift.kind', 'S10')
    day11 = fields.Many2One('staff.shift.kind', 'S11')
    day12 = fields.Many2One('staff.shift.kind', 'S12')
    day13 = fields.Many2One('staff.shift.kind', 'S13')
    day14 = fields.Many2One('staff.shift.kind', 'S14')
    day15 = fields.Many2One('staff.shift.kind', 'S15')
    day16 = fields.Many2One('staff.shift.kind', 'S16')


class PlanShiftsWeek(ModelSQL, ModelView):
    'Plan Shifts Week'
    __name__ = 'surveillance.plan.shift_week'
    _rec_name = 'sequence'
    line = fields.Many2One('surveillance.plan.line', 'Line', required=True)
    sequence = fields.Integer('Sequence', required=True)
    guard = fields.Many2One('company.employee', 'Guard')
    position = fields.Many2One('surveillance.location.position', 'Position',
        domain=[
            ('location', '=', Eval('_parent_line', {}).get('location')),
        ])
    monday = fields.Many2One('staff.shift.kind', 'Monday')
    tuesday = fields.Many2One('staff.shift.kind', 'Tuesday')
    wednesday = fields.Many2One('staff.shift.kind', 'Wednesday')
    thursday = fields.Many2One('staff.shift.kind', 'Thursday')
    friday = fields.Many2One('staff.shift.kind', 'Friday')
    saturday = fields.Many2One('staff.shift.kind', 'Saturday')
    sunday = fields.Many2One('staff.shift.kind', 'Sunday')
    holiday = fields.Many2One('staff.shift.kind', 'Holiday')
